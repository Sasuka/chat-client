import Vue from 'vue'
import Vuex from 'vuex'
import UserResource from "./api/user";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        currentUser: {},
        users: []
    },
    mutations: {
        SET_CURRENT_USER(state, user) {
            state.currentUser = user;
            // window.localStorage.currentUser = JSON.stringify(user);
            // let userStore = user ? JSON.stringify(user) : null;
            localStorage.setItem('currentUser', JSON.stringify(user));
        },

        LOGOUT_USER(state) {
            state.currentUser = JSON.stringify({});
            localStorage.setItem('currentUser', JSON.stringify({}));
        }
    },
    actions: {
        loginUser({commit}, loginInfo) {
            return new Promise(function (resolve, reject) {
                new UserResource('users').login(loginInfo)
                    .then((result) => {
                        if (result.code === 200) {
                            commit('SET_CURRENT_USER', result.data);
                            return resolve(result);
                        } else
                            return reject(result);
                    }).catch((error) => {
                    // console.log(error);
                    return reject(error);
                })
            })
        },

        registerUser({commit}, registerInfo) {
            return new Promise(function (resolve, reject) {
                new UserResource('users').register(registerInfo)
                    .then((result) => {
                        if (result.code === 200) {
                            commit('SET_CURRENT_USER', result.data);
                            return resolve(result);
                        } else
                            return reject(result);

                    }).catch((error) => {
                    // console.log(error);
                    return reject(error);
                })
            })
        },

        logoutUser({commit}) {
            commit('LOGOUT_USER');
        },

        async loadCurrentUser({commit}) {
            await commit('SET_CURRENT_USER', JSON.parse(localStorage.getItem('currentUser')));
        }
    }
})
