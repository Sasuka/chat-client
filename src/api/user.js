import request from '../utils/request'
import Resource from './resource';


class UserResource extends Resource {

    login(params) {
        return request({
            url: '/' + this.uri + '/login',
            method: 'post',
            data: params
        });
    }

    register(params) {
        return request({
            url: '/' + this.uri + '/register',
            method: 'post',
            data: params
        })
    }
}

export default UserResource;
