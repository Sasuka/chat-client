import UserLogin from "../views/UserLogin";
import UserRegistration from "../views/UserRegistration";

export default [
    {
        path: '/',
        name: 'home',
        component: () => import(/* webpackChunkName: "about" */ '../views/Home.vue')
    },
    {
        path: '/about',
        name: 'about',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
    },
    {
        path: '/login',
        name: 'user-login',
        component: UserLogin
    },
    {
        path: '/registration',
        name: 'user-registration',
        component: UserRegistration
    },
    {
        path: '/room/:name',
        name: 'room/chat',
        props: true,
        component: () => import('../views/RoomChat.vue')
    }
]
