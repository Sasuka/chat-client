import UserService from './user';

const services = {
    userController: new UserService()
};

export default services;
