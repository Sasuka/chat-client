"use strict";

import UserResource from "../api/user";

class UserService {

    constructor() {
        this.userService = new UserResource('users');
    }

    async loginUserSer(loginInfo) {
        try {
            await this.userService.login(loginInfo).then(result => {
                console.log(result.data);
                return result.data;
            }).catch(err => {
                alert('serv 2 '+ err);
                return "error";
            })
        } catch (e) {
            return {error: "Email/password combination was incorrect.  Please try again." + e}
        }
    }

}

export default UserService;
