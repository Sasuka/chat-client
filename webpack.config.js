const path = require('path');

module.exports = {
    devServer: {
        mode: 'production',
        compress: true,
        inline: true,
        port: '8080',
        disableHostCheck: true,
        entry: path.resolve(__dirname, 'src') + 'main.js',
        output: {
            /*Webpack producing results*/
            path: path.resolve(__dirname, "../src/dist"),
            filename: "app-bundle.js"
        }
    }
};
